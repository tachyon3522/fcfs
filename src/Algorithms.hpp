#ifndef ALGORITHMS_H
#define ALGORITHMS_H

#include <iostream>
#include <algorithm>
#include <set>
#include <functional>
#include <iomanip>
#include "Process.hpp"
#include "utils.hpp"

namespace Algorithms 
{
    // simulates FCFS sheduling AND returns complete process queue snapshot
    template <template <class...> class TContainer>
    auto simulate_FCFS(const TContainer<OS::Process> & process_list)
    {
        std::vector<OS::Process> executed_process_list;
        std::set<OS::Process, OS::sort_by_arrival_time_asc> at_sorted_process_list(process_list.begin(), process_list.end());
        
        int current_time = (*at_sorted_process_list.begin()).get_arrival_time();
        std::vector<int> current_times_list = {current_time};

        // function that returns request queue as vector
        auto get_process_queue = [&] (auto current_proc_it, int & current_time) {
            std::vector<OS::Process> request_queue;
            for (auto it = current_proc_it; it != at_sorted_process_list.end(); ++it) {
                if (it->get_arrival_time() <= current_time) {
                    request_queue.push_back(*it);
                }
            }
            return request_queue;
        };

        // function that print request queue id's
        auto print_request_queue_ids = [] (const auto & request_queue) {
            for (auto process : request_queue) {
                std::cout << process.get_id() << " ";
            }
            std::cout << std::endl;
        };

        // function that prints request queue as blanks
        auto print_request_queue_as_blanks = [] (const auto & request_queue) {
            for (int i = 0; i < request_queue.size(); ++i) {
                std::cout << "--";
                if (i < request_queue.size() - 1)
                    std::cout << ",";
            }
            std::cout << std::endl;
        };

        // function that prints executed process' ids 
        auto print_executed_process_ids = [] (const auto & executed_process_list) {
            for (const auto process : executed_process_list) {
                std::cout << "| " << process.get_id() << " ";
            }
            std::cout << "|";
            std::cout << std::endl;
        };

        // function that prints exceuted processes execution time as boxes
        auto print_executed_process_exec_time_as_box = [] (const auto & executed_process_list) {
            for (const auto process: executed_process_list) {
                // std::wcout << "| " << std::wstring(process.get_execution_time(), '\u9622') << " ";
                std::cout << "| " << std::string(process.get_execution_time(), '*') << " ";
            }
            std::cout << "|";
            std::cout << std::endl;
        };


        // iterate all processes according to arrival time
        for (auto current_proc_it = at_sorted_process_list.begin(); current_proc_it != at_sorted_process_list.end(); ++current_proc_it) {
            executed_process_list.push_back(*current_proc_it);
            std::vector<OS::Process> request_queue = get_process_queue(current_proc_it, current_time);

            // DISPLAYS
            std::cout << "AT TIME: " << current_time << "\n";
            std::cout << "Request Queue << ";
            print_request_queue_ids(request_queue);
            print_request_queue_as_blanks(request_queue);
            for (const auto & process : request_queue) {
                std::cout << std::setfill('0') << std::setw(2) << process.get_execution_time() << " ";
            }
            std::cout << "\n\n";

            std::cout << ">> EXECUTE PROCESS " << current_proc_it->get_id() << "\n";
            std::cout << "CPU:\n";
            print_executed_process_ids(executed_process_list);
            print_executed_process_exec_time_as_box(executed_process_list);
            for (const auto & time : current_times_list) {
                std::cout << std::setfill('0') << std::setw(2) << time << " ";
            }

            current_time += current_proc_it->get_execution_time(); 
            current_times_list.push_back(current_time);

            std::cout << std::endl << std::endl << std::endl ;
        }
        return std::vector<OS::Process> (at_sorted_process_list.begin(), at_sorted_process_list.end());
    }


    // display Turn Around Time 
}





#endif
