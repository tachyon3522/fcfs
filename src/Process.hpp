#ifndef PROCESS_HPP
#define PROCESS_HPP

#include <iostream>

namespace OS {
    class Process
    {
    public:
        Process(std::string id, int arrival_time, int execution_time) : 
            _id(id),
            _arrival_time(arrival_time),
            _execution_time(execution_time)
        {   }

        Process(int arrival_time, int execution_time) : 
            _id("P" + std::to_string(id_counter++)),
            _arrival_time(arrival_time),
            _execution_time(execution_time)
        {   }

        std::string get_id() const { return _id; }
        int get_arrival_time() const { return _arrival_time; }
        int get_execution_time() const { return _execution_time; }

    private:
        static int id_counter;

        std::string _id;
        int _arrival_time;
        int _execution_time;
    };

    struct sort_by_arrival_time_asc
    {
        bool operator () (const Process & left_process, const Process & right_process)
        {
            return left_process.get_arrival_time() < right_process.get_arrival_time();
        }
    };

    bool sort_bt_at_asc (const Process left_process, const Process right_process) 
    {
        return left_process.get_arrival_time() < right_process.get_arrival_time();
    }
}

#endif   