#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <unordered_map>
#include "Process.hpp"

namespace Utils {

    // prints {symbol} {rep} times
    void reprint(int rep, std::string symbol) 
    {
        for (int i = 0; i < rep; ++i) {
            std::cout << symbol;
        }
    }

    // displays message then returns generic outputs 
    template <typename T>
    T get_input(std::string message)
    {
        T value;
        std::cout << message;
        std::cin >> value;

        return value;
    }

    // displays the process inputs in a table format
    template <template <class...> class TContainer>
    void display_process_table(TContainer<OS::Process> &process_list)
    {
        std::unordered_map<std::string, std::string> Headers {{"EXECUTION", " EXEC TIME "},
                                                              {"ARRIVAL", " AT "},
                                                              {"ID", " PRO "} };

        auto print_border = [&] (bool end_with_line = true) {
            // above borders
            std::cout << "+" << std::string( Headers["ID"].length(), '-'); // PRO border
            std::cout << "+" << std::string( Headers["ARRIVAL"].length(), '-'); // AT border
            std::cout << "+" << std::string( Headers["EXECUTION"].length(), '-'); // EXEC TIME border
            std::cout << "+"; // end of border
            std::cout << "\n"; // next line (to inputs line)
            if (end_with_line)
                std::cout << "|"; // start of inputs line
        };

        // display headers
        print_border();
        for (auto const & header : Headers) {
            std::cout << header.second << "|";
        }
        std::cout << std::endl;

        // top to bottom iteration
        for (int i = 0; i < process_list.size(); ++i) {

            print_border(); // prints above borders before input lines
            // inputs line
            std::cout << std::setw(Headers["ID"].length()) << process_list[i].get_id() << "|";
            std::cout << std::setw(Headers["ARRIVAL"].length()) << process_list[i].get_arrival_time() << "|";
            std::cout << std::setw(Headers["EXECUTION"].length()) << process_list[i].get_execution_time() << "|";

            // end of left to right iteration
            std::cout << std::endl;
        }

        // bottom border
        print_border(false); // false means do not print line after border
    }
}

#endif