#include <iostream>
#include <vector>

#include "src/Process.hpp"
#include "src/utils.hpp"
#include "src/Algorithms.hpp"


int main(int argc, char *argv[]) {
    int process_count = Utils::get_input<int>("Enter number of processes to be evaluated: ");
    std::vector<OS::Process> process_list;


    for (int i = 0; i < process_count; ++i) {

        std::string id = "P" + std::to_string(i);
        int arrival_time = Utils::get_input<int>("Input AT of " + id + ": ");
        int execution_time = Utils::get_input<int>("Input ET of " + id + ": ");
        std::cout << std::endl;

        process_list.push_back(OS::Process(id, arrival_time, execution_time));
    }
    Utils::display_process_table(process_list);
    Algorithms::simulate_FCFS(process_list);
    

    std::cout << std::endl;
}